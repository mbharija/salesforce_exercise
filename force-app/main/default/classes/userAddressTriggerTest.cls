/**
 * @description       : This class is to Test the Trigger called on Updation of Street Address in User Object
 * @author            : Mitali Bharija 
 * @last modified on  : 11-18-2021
 * @last modified by  : Mitali Bharija
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0  11-18-2021  Mitali Bharija   Initial Version
**/

@isTest
public class userAddressTriggerTest {

    @isTest 
    private static void testUpdateUserAddress()
    {
       	 User adminUser = [SELECT Id FROM User WHERE profile.name =: 'System Administrator' and isActive=true Limit 1];
       // Run in System Admin context in this case.This is used to bypass Mixed DML Exception 
        System.runAs (adminUser) {
        Test.startTest();
        // Call the TestData Setup method from Utility Class
        List<User> userList = userTestUtility.testDataSetup();
        insert userList;
        List<Id> contactIdsList = new List<Id>();
        // Iterate over userList to update Street address and get the list of Contact Ids which needs to be updated
        for (User user: userList )
        {
            user.Street='Potomac Road';
            System.debug('The modified User Address Is :: '+user.street);
            contactIdsList.add(user.ContactId);
        }
         // This will Update the User with Street Address and will fire the After Trigger to update Contact mailing Address
        update userList;
        for(Id contactId :contactIdsList)
        {
            Contact c= [Select Id,MailingStreet from Contact where Id=: contactId];
             System.assertEquals('Potomac Road', c.MailingStreet, 'ERROR');
        }
        Test.stopTest();
    }
    }
    
  
}