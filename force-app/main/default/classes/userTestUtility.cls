/**
 * @description       : This class is a utility class to setup Test Data
 * @author            : Mitali Bharija 
 * @last modified on  : 11-18-2021
 * @last modified by  : Mitali Bharija
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   11-18-2021   Mitali Bharija   Initial Version
**/
public with Sharing class userTestUtility {
    
    public static List<User> testDataSetup()
    {
        List<User> userList = new List<User>();
        Profile p = [select id from Profile where name='Customer Community Plus User' limit 1];
        // Insert Test Account
        Account acc = new Account(name='TestAccount');
        insert acc;
        List<Contact> contactsList = new List<Contact>();
        for(Integer i=0;i<1;i++)
        {
            Contact c = new Contact(lastName='TestContact'+i,AccountId=acc.Id);
            contactsList.add(c);
        }
        // insert Test Contacts
        insert contactsList;
        for(Contact c :contactsList)
        {
            User a = new User(alias = 'myTest', email='mitiTestUser@gmail.com',
                 emailencodingkey='UTF-8', firstName='Mitali', lastname='Bharija',languagelocalekey='en_US',
                 localesidkey='en_US', profileid = p.id,contactId=c.Id,
                 timezonesidkey='America/Los_Angeles',  username=System.now().millisecond()+'test@gmail.com');
            userList.add(a);
        }
        // Return the User List which is created
        return userList;
     
    }
    

}