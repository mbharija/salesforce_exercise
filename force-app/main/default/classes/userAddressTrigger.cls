trigger userAddressTrigger on User (after Update) {
   		// Check if the Trigger is After Update Trigger
        if(Trigger.isAfter && Trigger.isUpdate)
        {
          for(User newUser :Trigger.New)
          {
              //Fetching the Old User details from OldTriggerMap by passing ID
              User oldUserData = Trigger.oldMap.get(newUser.Id);
              // Calling the Trigger Handler only if the Street Address is changed
              if(oldUserData.street!=newUser.street)
              {
                System.debug('Street Address has been updated');
                  //Call the Trigger Handler to update the Mailing address in Contact sobject
               userAddressTriggerHandler.updateContactAddress(Trigger.New);
              }
          }        
        }
}