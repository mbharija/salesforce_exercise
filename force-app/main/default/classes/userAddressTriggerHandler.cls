/**
 * @description       : This class is a Handler for userAdressTrigger
 * @author            : Mitali Bharija 
 * @last modified on  : 11-18-2021
 * @last modified by  : Mitali Bharija
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   11-18-2021   Mitali Bharija   Initial Version
**/
public class userAddressTriggerHandler {
    
    public static void updateContactAddress(List<User> updatedUserList)
    {
        List<contact> contactList = new List<contact>();
        Map<Id,User> userUpdatedMap = new Map<Id,User>();
        
        try{
            //Create a Map with Contact Id from User as Key and the User sObject as the value
            for(User updatedUser :updatedUserList)
            {
                userUpdatedMap.put(updatedUser.contactId,updatedUser);
            }
            // Null check for the Map
            if(userUpdatedMap !=null)
            {
                // Iterating over the Map Key set and get the Contact record
                for(Contact c : [Select Id,MailingStreet from Contact where Id in:userUpdatedMap.keySet()])
                {
                    if(c!=null)
                    {
                      // Update Contact Mailing Street address with the user Street
                      c.MailingStreet = userUpdatedMap.values().street;
                    contactList.add(c);  
                    }    
                }
                update(contactList);
            }
        }
        catch(Exception e)
        {
            throw e;
        }
    }

}